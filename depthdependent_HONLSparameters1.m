function [h,k,sigma,cg,cp,lambda,nu,alpha,alpha4,beta,gamma,mu,muintexp] = depthdependent_HONLSparameters1(omega,kh,x,FOD)
    %%
    % input params 
    % omega is the angular frequency in units of g^{1/2}
    % hk is the current value of h(x)k(x)
    % x is the position along the propagation distance
    % 
    % output params
    % h(x) depth
    % k(x) wavenumber
    % sigma = tanh(kh)
    % cg = group velocity
    % cp = phase velocity
    
    %
    % The shoaling is trivial, see Djordjevic&Redekopp ZAMP 1978
    % muintexp is the exp of the integral of mu(x) exp(int(mu))
    %
    % The parameters are defined as in Sedletsky JETP Lett. 97, 180-193 (2003)
    % where the envelope of the surface elevation is used and the expansion
    % to fourth order is performed
    %
    % lambda = group velocity dispersion
    % alpha = third order dispersion (=0 in the limit of deep water)
    % alpha4 = fourth order dispersion
    % nu = cubic nonlinear coefficient
    % beta = coefficient of |A|^2 D_t A
    % gamma = coefficient of A D_t |A|^2
    %
    % Andrea ARMAROLI, GAP UniGE, 25/02/2019
    %%
    
    % auxiliary quantities
    sigma = tanh(kh);
    k = omega.^2./sigma;
    h = kh.*sigma./omega^2;
    % group and phase speed
    cp = omega./k;
    cg = (sigma + kh.*(1-sigma.^2))/2./omega;
    
    % nu in the paper
    sigma2 = (2*omega*cg).^2 - 4 .* kh .* sigma;
    sigmaq = sigma.^2-1;
    % mu in the paper
    sigma3 = sigmaq.^2.*kh - sigma.*(sigma.^2-5);
    % NLS quantities
    % beta''/2 = omega''/Cg^3/2
    lambda = -(1 - h./cg.^2.*(1-kh.*sigma).*(1-sigma.^2))./(2*omega.*cg);
    % alphaT = -omega'''/6
    alphaT = omega./(48.*k.^3.*sigma.^3).*((sigma.^2-1).*(15.*sigma.^4-2.*sigma.^2 + 3).*kh.^3 - ...
        3.*sigma.*(sigma.^2-1).*(3*sigma.^2+1).*kh.^2-...
        3.*sigma.^2.*(sigma.^2-1).*kh - 3.*sigma.^3);
    % changing to space evolution
    % TOD
    % beta3/6 = alphaT/cg^4 + 2*lambda^2*cg
    alpha = alphaT./cg.^4 + 2 * lambda.^2.*cg;
    % FOD
    % beta4/24
    if FOD ~=0
        alpha4 = -1./(3*128*omega.^7.*cg.^7).*h.*sigmaq.*(...
            - 3*kh.^4.*sigmaq.^4.*(1-5.^kh)  ...
            - 4.*kh.^sigma.^3.*sigmaq.*(1-kh.*sigma).*(3-4*kh.*sigma).*(5-8.*kh.*sigma)...
            - 4.*kh.^3.*sigma.*sigmaq.^3.*(15+kh.*sigma.*(-31+24.*kh.*sigma))...
            + sigma.^4.*(-3 + kh.*sigma.*(39+8.*kh.*(-7+2*kh.*sigma)))...
            + 6.*kh.^2.*sigma.^2.*sigmaq.^2.*(-19+kh.*sigma.*(51+4*kh.*sigma.*(-11 + 4.*kh.*sigma))));
    else 
        alpha4 = zeros(size(kh));
    end
    % cubic nonlinearity
    nu = k.^2.*omega./(16*sigma.^4.*cg).*(9 - 10*sigma.^2 + 9*sigma.^4-...
        2.*sigma.^2.*cg.^2./(h-cg.^2).*(4*cp.^2./cg.^2 + 4* cp./cg.*(1-sigma.^2) + h./cg.^2.*(1-sigma.^2).^2));
    % 
    % 4O coefficients
    Q41tilde = 1./(16.*sigma.^5.*sigma2).*((2*sigma.^6 - 11 .* sigma.^4 - 10*sigma.^2 + 27).*sigmaq.^3.*kh.^3 - ...
        sigma.*sigmaq.*(6.*sigma.^8-21.*sigma.^6 +9 .*sigma.^4 - 43.*sigma.^2 + 81).*kh.^2 - ...
        sigma.^3.*(sigma.^2+1).*(2*sigma.^4-7*sigma.^2-27)+...
        sigma.^2.*kh.*(6*sigma.^8-15*sigma.^6-77*sigma.^4+71*sigma.^2 -81));
    Q42tilde = 1./(32.*sigma.^5.*sigma2).*(-(4*sigma.^6 + 5.* sigma.^4 - 10*sigma.^2 + 9).*sigmaq.^3.*kh.^3 + ...
        sigma.*sigmaq.*(12.*sigma.^8 - 45.*sigma.^6 +71 .*sigma.^4 - 15.*sigma.^2 + 9).*kh.^2 + ...
        sigma.^3.*(4*sigma.^6 - 43*sigma.^4 + 118*sigma.^2 + 9) ...
        -sigma.^2.*kh.*(12*sigma.^8-93*sigma.^6+215*sigma.^4-111*sigma.^2+9));
    % Slunyaev correction
    DeltaS = -1./(16.*sigma.^3.*sigma2).*(sigmaq.^4.*(3*sigma.^3+1).*kh.^3 ...
        - sigma.*sigmaq.^2.*(5.*sigma.^4 - 18 *sigma.^2 - 3).*kh.^2 ...
        + sigma.^2.*sigmaq.^2.*(sigmaq-9).*kh ...
        + sigma.^3.*sigmaq.*(sigma.^2-5));
    % overall |A|^2 D_x A term 
    Q41 = 1./(32.*sigma.^5.*sigma2.^2).*(...
        sigmaq.^5.*(3.*sigma.^6-20.*sigma.^4-21.*sigma.^2 +54).*kh.^5 ...
        - sigma.*sigmaq.^3.*(11.*sigma.^8-99.*sigma.^6-61.*sigma.^4+7.*sigma.^2+270).*kh.^4  ...
        + 2*sigma.^2.*sigmaq.*(7.*sigma.^10-58.*sigma.^8+38.*sigma.^6+52.*sigma.^4-181.*sigma.^2+270).*kh.^3  ...
        - 2.*sigma.^3.*(3.*sigma.^10 + 18.*sigma.^8 - 146.*sigma.^6 - 172.*sigma.^4 + 183.*sigma.^2 - 270).*kh.^2  ...
        - sigma.^4.*(sigma.^8 - 109.*sigma.^6 + 517.*sigma.^4 + 217.*sigma.^2 + 270).*kh ...
        + sigma.^5.*(sigma.^6 - 40.*sigma.^4 + 193.*sigma.^2 + 54) ...
        ) + DeltaS;
    betaT = omega.*k.*Q41;   
    % change to time evolution variable
    % |A|^2 D_t A term
    beta = betaT./cg.^2 - 4 * lambda.*cg.*nu;
    
    % A^2 D_x A* term
    Q42 = 1./(32.*sigma.^5.*sigma2.^2).*(...
        - sigmaq.^5.*(3.*sigma.^6 + 7.*sigma.^4-11.*sigma.^2 + 9).*kh.^5 ...
        + sigma.*sigmaq.^3.*(11.*sigma.^8-48.*sigma.^6 + 66.*sigma.^4+8.*sigma.^2+27).*kh.^4  ...
        - 2*sigma.^2.*sigmaq.*(7.*sigma.^10-79.*sigma.^8+ 282.*sigma.^6 - 154.*sigma.^4 - sigma.^2+9).*kh.^3  ...
        + 2.*sigma.^3.*(3.*sigma.^10 - 63.*sigma.^8 + 314.*sigma.^6 - 218.*sigma.^4 + 19.*sigma.^2 + 9).*kh.^2  ...
        + sigma.^4.*(sigma.^8 + 20.*sigma.^6 - 158.*sigma.^4 - 28.*sigma.^2 - 27).*kh  ...
        - sigma.^5.*(sigma.^6 - 7.*sigma.^4 + 7.*sigma.^2 - 9) ...
        ) - DeltaS;
    gammaT = omega.*k.*Q42;
    % change to time evolution variable
    % A D_t |A|^2 term
    gamma = gammaT./cg.^2 - 2 * lambda.*cg.*nu;
    
    % shoaling (depth-related loss/amplification)    
    mu = (1-sigma.^2).*(1-kh.*sigma)./(sigma+kh.*(1-sigma.^2));
    muintexp = ((2.*kh + sinh(2*kh))./cosh(kh).^2).^0.5;
%     muintexp = cg.^.5;
    
end