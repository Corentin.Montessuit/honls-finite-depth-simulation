function [u4,err] = DVRK34_2(hx,u,omega,lambda0,lambda1,lambda2,nu0,nu1,nu2,dis)
% adaptive embedded RK4(3) method for solving the variable depth NLS in the Interaction Picture
% INPUT
% hz: current step size
% u: complex field
% omega: frequency bins
% lambda0, 1, 2 are the values of lambda at x, x+hx/2, x + hx
% nu0, 1, 2 are the values of nu at x, x+hx/2, x + hx
% cg0, 1, 2 are the values of Cg at x, x+hx/2, x + hx
% shoalflag allows to switch shoaling off (it could be useful to compare to fibres)
% In the present normalization the shoaling enters the nonlinear
% coefficient
%
% BEWARE the IP integrating factor now involves an integral of lambda in
% the expontential and the multiplication by a factor on account of
% shoaling.
%
% OUTPUT: function value at z + hz, correcting hz0 accordingly
%
% New normalization (no loss/gain by shoaling, but nu incorporates Cg from the main) 
% 16/06/19
%
persistent k5
n = size(omega,1);
mask = ones(n,1);
% mask(round(6*n/14:8*n/12-1)) = 0;

 % integrating factor x -> x + hx/2
    phaselin1 = exp(-1i.*omega.^2.*(lambda0+lambda1)./2.*hx/2);
    
    % integrating factor x+ hx/2 -> x + hx
    phaselin2 = exp(-1i.*omega.^2.*(lambda2+lambda1)./2.*hx/2);
  
uIP = fft((phaselin1.*(ifft(u).*mask)));
if isempty(k5)
    Ncurr = nonlinear(u, nu0, dis);
    k1 = fft(phaselin1.*(ifft(Ncurr)));
else
    k1 = fft(phaselin1.*(ifft(k5)));
end

k2 = nonlinear(uIP + hx/2.*k1 , nu1,dis);
k3 = nonlinear(uIP + hx/2.*k2 ,  nu1,dis);
u2 = fft(phaselin2.*(ifft(uIP + hx.*k3)));
k4 = nonlinear(u2,  nu2,dis);
beta = fft(phaselin2.*ifft(uIP + hx/6.*(k1 + 2*k2 + 2*k3)));
u4 = beta + hx/6*k4;
k5 = nonlinear(u4,  nu2,dis);
u3 = beta + hx/30.*(2*k4+3*k5);
err = norm(u3-u4);


end 

function [uNL] = nonlinear(u, nu, d)
% if nu<0
%     NLflag = 0;
% else
    NLflag = 1;
% end
% uNLDysthe = 8.*abs(u).^2.*fft(-1i.*omega.*ifft(u)) + beta*2.*u.^2.*fft(-1i.*omega.*ifft(conj(u))) + 2i.*u.*fft(abs(omega).*ifft(abs(u).^2));
uNL = -NLflag*1i.*nu*abs(u).^2.*u - d.*u;
end


