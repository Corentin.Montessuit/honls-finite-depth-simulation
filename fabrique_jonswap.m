% generation d un signal Jonswap pour le nouveau batteur 
% en tenant compte de la fonction de transfert

clear all; close all ;
% input parameters
tfinal = 1*600;   % duration  du signal g�n�r� en s     
% Hs1    = 0.105;   %en m  SWH in m
% Tp1 = 1.05   %s
% nbT  = 1.8;  %   frequences entre 0 et nbT*Fp1  Hz
% Tp1    = 1.25   %s    Peiod
% nbT    = 2.1;  %   frequences entre 0 et nbT*Fp1  Hz
%   nbT / Tp1  should be less than 2.0 (Hz) 
gamma  = 3.3;
new       = 1 ;   % new=1 si on genere un nouveau jonswap
for n=1:10
for Hs1 = [0.04]
    for nbT = [1.6]
        for Tp1 = [0.8]

   if new == 1  
          fileJ=['GJ_NORAO_TP',num2str(floor(Tp1)),'p',num2str(rem(100.*Tp1,100)),...
                 'Hs',num2str(round(1000.*Hs1)), 'mm_',num2str(round(tfinal./60)),'min.txt']
          fileB=['GJ_RAO_TP_',num2str(floor(Tp1)),'p',num2str(rem(100.*Tp1,100)),...
                '_Hs_',num2str(round(1000.*Hs1)), '_nbT_',num2str(floor(nbT)),'p',num2str(rem(100.*nbT,100)),...
                '_gamma_',num2str(floor(gamma)),'p',num2str(rem(100.*gamma,100)),...
                '_n_',num2str(n),'.txt']
   end



nptbl=8192; 


hi     = 0.75 ;        % profondeur en m
offset = 5.00 ;    %position milieu batteur en Volt


Fp1 = 1./Tp1  %Hz

%nbraie = 2600;    % nbraie frequences entre 0 et nbT*Fp1  Hz
nbraie = 5000;    % nbraie frequences entre 0 et nbT*Fp1  Hz
 sigp = 0.09; sigm=0.07;

fa  = 400;    % generation �  fa Hz


% calcul spectre jonswap formule vraie 
    Tp=Tp1
    Hs=Hs1
    fo=1/Tp; omega0=2*pi*fo;
    ff=0:nbT*fo/(nbraie-1):nbT*fo;
    omega = 2*pi*ff;
    alpha = 1;
    ifm   = find(ff<fo); ifp=find(ff>=fo);
    omegam= omega(ifm); omegap=omega(ifp);
    Gp    = exp(-(omegap-omega0).^2/2/sigp.^2./omega0.^2);
    EJp   = alpha.*Hs^2*omegap.^-5.*omega0.^4.*exp(-5.*(omegap./omega0).^-4./4).*gamma.^Gp;
    Gm    = exp(-(omegam-omega0).^2/2/sigm.^2./omega0.^2);
    EJm   = alpha.*Hs^2*omegam.^-5.*omega0.^4.*exp(-5.*(omegam./omega0).^-4./4).*gamma.^Gm;
    EJm(1)= 0;
    clear EJ 
    EJ    = [EJm EJp];
    alpha = Hs^2/16/(sum(EJ(1:end-1)+EJ(2:end))/2)/ff(2);
    EJ    = alpha*EJ;

    t=0:1/fa:tfinal;
    nbpts=length(t);
    s=0;
  
   
    % fonction de transfert  bernard cot� batteur
    % attention  pour les spectres, il s agit des amplitudes au carr� 

    load TRANSFERT_OLIVE_CMVAGUE_PAR_CMBATTEUR_PERIOD
    RAO_B =    TRANSFERT_OLIVE_CMVAGUE_PAR_CMBATTEUR_PERIOD(:,2);   
    FRAO  =    1./TRANSFERT_OLIVE_CMVAGUE_PAR_CMBATTEUR_PERIOD(:,1);   % frequence
    for i=1:3
        I=find( FRAO(2:end) == FRAO(1:end-1) )
        FRAO(I)=FRAO(I).*1.001;
    end
        
    
    %these lacaze    
%     load RAObatteurbernard.txt;  % col 1 : periode (s)    col2 : RAO
%     RAO_B =    RAObatteurbernard(:,2);   
%     FRAO  = 1./RAObatteurbernard(:,1);   % frequence
    
    %spline: on a x et y, on spline sur xx et yy
    raoB    = spline(FRAO,RAO_B,ff(2:end)); raoB=[0  raoB];
    I       = find( raoB <= 0.46 ); raoB(I)=  0.46;

    figure(1);
    subplot(211);
    plot(ff,raoB,'r');hold on;
    tt=title('RAO  (amplitude)');set(tt,'fontsize',14);
    set(gca,'fontsize',16);
    tt=xlabel('f (HZ)');set(tt,'FontSize',14);
    tt=ylabel('RAO  (amplitude)');set(tt,'FontSize',14);
    axis([0 5 0 4 ])
    subplot(212)
    plot(ff,1./raoB,'r');hold on;
    tt=title('1./RAO  (amplitude)');set(tt,'fontsize',14);
    set(gca,'fontsize',16);
    tt=xlabel('f (HZ)');set(tt,'FontSize',14);
    tt=ylabel('1/RAO  (amplitude)');set(tt,'FontSize',14);
    %axis([0 5 0 4 ])
    
    figure(2);
    subplot(211)
    plot(ff,(raoB.*raoB),'r');hold on;
    tt=title(' RAO  (energie)');set(tt,'fontsize',14);
    set(gca,'fontsize',16);
    tt=xlabel('f (HZ)');set(tt,'FontSize',14);
    tt=ylabel('RAO (energie)');set(tt,'FontSize',14);
    axis([0 5 0 4 ])
    subplot(212)
    plot(ff,1./(raoB.*raoB),'r');hold on;
    tt=title(' 1/RAO  (energie)');set(tt,'fontsize',14);
    set(gca,'fontsize',16);
    tt=xlabel('f (HZ)');set(tt,'FontSize',14);
    tt=ylabel('1/RAO (energie)');set(tt,'FontSize',14);

  % On applique la fonction de transfert
  EJBatteur=EJ;
  EJBatteur(2:end)=EJBatteur(2:end)./(raoB(2:end).*raoB(2:end));  % au carr� car energie
  
 
  figure(3);
  loglog(ff,EJ,'linewidth',1);
  hold on;
  loglog(ff,EJBatteur,'r','linewidth',2);
  set(gca,'fontsize',18);
  legend('Jonswap','Jonswap/RAO');
  xlabel('F (Hz)');
  ylabel('m2/Hz');
    axis([0.2 4 1e-7 1e-2])

    
if new==1
        for ii=1:length(ff)
          phi(ii)=rand(1)*2*pi;
          aBatteur(ii)=sqrt(EJBatteur(ii)*omega(2));
          aJonswap(ii)=sqrt(EJ(ii)*omega(2));
          
           %%%if ii~=1 k(ii)=resolwkh(hi,1/ff(ii)); else k(ii)=0; end;
         end;
    %%%save JONSW   aBatteur aJonswap k phi ff  EJBatteur  EJ 
else
    %load JONSW
    %load jonswap20minT1000ms
    %load J_NORAO_TP1p2.5Hs11cm_31min.txt
end

    
    
    sBatteur=zeros(length(t),1);
    sJonswap=zeros(length(t),1);
        for jj=1:length(ff)
            jj
            sBatteur=sBatteur+aBatteur(jj)*sin(2*pi*ff(jj)*t+phi(jj))';
            sJonswap=sJonswap+aJonswap(jj)*sin(2*pi*ff(jj)*t+phi(jj))';
        end; 
    
%     x=0;
%     sBatteur=zeros(length(t),length(x));
%     sJonswap=zeros(length(t),length(x));
%     for ii=1:length(x);
%         ii
%         for jj=1:length(ff)
%             sBatteur(:,ii)=sBatteur(:,ii)+aBatteur(jj)*sin(2*pi*ff(jj)*t-k(jj)*x(ii)+phi(jj))';
%             sJonswap(:,ii)=sJonswap(:,ii)+aJonswap(jj)*sin(2*pi*ff(jj)*t-k(jj)*x(ii)+phi(jj))';
%         end; 
%     end;
%     
    
    nbrampe=10; iramp=round(nbrampe*Tp/t(2)); 
    ramp=sin(pi*([0:iramp])*t(2)/2./nbrampe/Tp);
    ramp=[ramp ones(length(t)-2*length(ramp),1)' fliplr(ramp)];
    sBatteur=sBatteur.*ramp';
    sJonswap=sJonswap.*ramp';
    
    %normalisation   on veut Hs signal =Hs demand�
    coeff= (Hs/4)./std(sJonswap);
    sJonswap=sJonswap.*coeff;
    sBatteur=sBatteur.*coeff;
   
%    nx=512*16
%    [pB,f1]=pwelch(sB,hanning(nx),nx/2-1,nx,fa);
%    [pL,f1]=pwelch(sL,hanning(nx),nx/2-1,nx,fa);
     


    nptbl=8192;
%     [SHB,F]=pspectrum(sBatteur,nptbl,nptbl/2,nptbl,fa) ;
%     SHB(:,1)=SHB(:,1)./( (F(2)-F(1)) * length(SHB(:,1)) )  ;
%     [SHJ,F]=pspectrum(sJonswap,nptbl,nptbl/2,nptbl,fa) ;
%     SHJ(:,1)=SHJ(:,1)./( (F(2)-F(1)) * length(SHJ(:,1)) )  ;
%       
% 
%     figure(5);  
%     plot(ff,EJ,'linewidth',2); hold on; 
%       hold on;
%     plot(ff,EJBatteur,'r','linewidth',2);
%     axis([0.2 4 0 5e-3])
%     plot(F,SHJ(:,1),'c','linewidth',2);
%     plot(F,SHB(:,1),'m','linewidth',2);
%     set(gca,'fontsize',18);
%     legend('Jonswap theorique','Jonswap theorique/RAO',...
%            'Jonswap genere','Jonswap genere/RAO');
%     fprintf('Tp = %2.2f, Hs= %2.2f\n',Tp,Hs);
%     tt=xlabel('f (HZ)');set(tt,'FontSize',18);
%     tt=ylabel('Spectrum  m2/Hz');set(tt,'FontSize',18);
%     grid
%     
%     figure(6);
%     loglog(ff,EJ./max(EJ),'linewidth',2); hold on; 
%       hold on;
%     loglog(ff,EJBatteur./max(EJBatteur),'r','linewidth',2);
%     axis([0.2 4 1e-4 10])
%     loglog(F,SHJ(:,1)./max(SHJ(:,1)),'c','linewidth',2);
%     loglog(F,SHB(:,1)./max(SHB(:,1)),'m','linewidth',2);
%     set(gca,'fontsize',18);
%     legend('Jonswap theorique','Jonswap theorique/RAO',...
%            'Jonswap genere','Jonswap genere/RAO');
%     fprintf('Tp = %2.2f, Hs= %2.2f\n',Tp,Hs);
%     tt=xlabel('f (HZ)');set(tt,'FontSize',18);
%     set(gca,'fontsize',18);
%     tt=ylabel(' m2/Hz');set(tt,'FontSize',18);
%         title(['file: ',fileJ]);     
    
    figure(7);
    loglog(ff,EJ,'linewidth',2); hold on; 
      hold on;
    loglog(ff,EJBatteur,'r','linewidth',2);
    axis([0.2 4 1e-6 1e-2])
%     loglog(F,SHJ(:,1),'c','linewidth',2);
%     loglog(F,SHB(:,1),'m','linewidth',2);
    set(gca,'fontsize',18);
    legend('Jonswap theorique','Jonswap theorique/RAO',...
           'Jonswap genere','Jonswap genere/RAO');
    fprintf('Tp = %2.2f, Hs= %2.2f\n',Tp,Hs);
    tt=xlabel('f (HZ)');set(tt,'FontSize',18);
    tt=ylabel(' m2/Hz');set(tt,'FontSize',18);
        title(['file: ',fileJ]);     


 
    % on transforme les m deplacement batteur  (sB) en volt :  10 v = 0.22 m
    VoltparM = 10./0.22 ;% pour luminy, on met le max � amax volt  et on ajoute l offset ;
    sBatteurVolt = sBatteur.* VoltparM;
    sJonswapVolt = sJonswap.* VoltparM;
    %sB=sB.* amax ./ max(abs(sB)) ;
    %ajout offset  ( 5 Volt)
    sBatteurVolt=sBatteurVolt+offset;
    sJonswapVolt=sJonswapVolt+offset;
    sigB=[t' sBatteurVolt];
%     %%eval(['save jonsw_Hs' num2str(Hs*1000) 'Tp' num2str(Tp*1000) '.txt sig -ascii']);
% % % %       file=['JonsTrans20minT', num2str(Tp*1000),'ms.txt'];
% % % %       eval(['save ',file,'  s -ascii']);
% %     
    

    
     
    if new == 1
         
%         eval(['save ',fileJ,' sJonswapVolt -ASCII  ;']);
    eval(['save ',fileB,' sBatteurVolt -ASCII  ;']);
         
    figure(10);
    subplot(212);
    plot(t,sBatteurVolt,'r');
    set(gca,'fontsize',16);
    xlabel('t (s)');
    ylabel('consigne (Volt) avec RAO');
    hold on;  
    axis([0 max(t) min(0,min(sBatteurVolt)) max(10,max(sBatteurVolt))]);legend('consigne avec RAO');
    grid
    title(['file: ',fileB]);
    subplot(211)
    plot(t,sJonswapVolt,'b');
    set(gca,'fontsize',16);
    xlabel('t (s)');
    ylabel('consigne (Volt) sans RAO');
    hold on;  
    axis([0 max(t) min(0,min(sBatteurVolt)) max(10,max(sBatteurVolt))]);legend('consigne sans RAO');
    grid    
    title(['file: ',fileJ]);     
     
     
     end
     
     if (max(sBatteurVolt) >= 8.5 || min(sBatteurVolt) < 0.2)
        fileB
        [max(sBatteurVolt) min(sBatteurVolt)]
%         print('Alarm!')
    else
        fileB
    end
    

% TT=sBatteurVolt;
% fid=fopen('essaivirgule.txt','w');
% %fid=fopen(file,'w');
% 
% for i=1:length(TT)
%     if mod(i,100)==0
%         i
%     end
%     S=num2str(TT(i));
%     j=NaN;
%     j=find(S=='.');
%     S(j)=',';
%     fprintf(fid,'%s  \n',S);
% end
% 
% fclose(fid);
        end
    end
end
end
