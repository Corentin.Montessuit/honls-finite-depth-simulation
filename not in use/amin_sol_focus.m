
clear all
close all
clc


t=-30:1/100:45;

h=0.324;
a0=0.012757;
k0=0.14/a0;
w0=sqrt(9.81*k0*tanh(k0*h))
f0=w0/(2*pi)
lambda=2*pi/k0
Steepness=a0*k0


x=0;
X=sqrt(2)*k0^2*a0*(x-(w0/(2*k0))*t);
T=-k0^2*w0*a0^2*t/4;
QBIS = 4*a0*((cosh(3*X)+3*cosh(X).*exp(8*i*T))./(cosh(4*X)+4*cosh(2*X)+3*cos(8*T))).*exp(i*T);


Amplitude = real(QBIS.*exp(i.*(k0.*x-w0.*t)));

plot(t,Amplitude,'r')

axis([-40  40 -0.08 0.08])

%%
t=-183:1/100:183;

a0=0.0072899;
k0=0.08/a0;
w0=sqrt(9.81*k0);
f0=w0/(2*pi)
lambda=2*pi/k0
Steepness=a0*k0

x=0;
X=sqrt(2)*k0^2*a0*(x-(w0/(2*k0))*t);
T=-k0^2*w0*a0^2*t/4;

QBIS = 4*a0*((cosh(3*X)+3*cosh(X).*exp(8*i*T))./(cosh(4*X)+4*cosh(2*X)+3*cos(8*T))).*exp(i*T);

Amplitude = real(QBIS.*exp(i.*(k0.*x-w0.*t)));

plot(t,Amplitude,'r')

axis([-40  40 -0.08 0.08])