clear all
close all
clc



h=0.324;
a=0.012757;   %f153s0.14,a=0.014797   %s0.14f165,a=0.012757 %s0.16f1650.01458
s=0.14;
%%
k=s/a
g=9.81;
q=k*h
w=sqrt(g*k*tanh(q));
frequency=w/(2*pi)
lambda=2*pi/k;
%%
c_g = (g.*tanh(h.*k) - g.*h.*k.*(tanh(h.*k).^2 - 1))/(2.*(g.*k.*tanh(h.*k)).^(1/2));
alpha = (-(c_g^2)/(2*w)+(w*h*cosh(q)^2)./(k*sinh(2*q))-h*c_g*tanh(q))/c_g^3;
%(-0.5.*-(g^2*(tanh(h*k)^2 + h^2*k^2 + 2*h*k*tanh(h*k)^3 - 2*h*k*tanh(h*k) + 2*h^2*k^2*tanh(h*k)^2 - 3*h^2*k^2*tanh(h*k)^4))/(4*(g*k*tanh(h*k))^(3/2)))/c_g^3;
beta = ((w.*k.^2)/(16.*(sinh(q)).^4).*(cosh(4.*q)+8-2.*(tanh(q)).^2)-((w/(2.*(sinh(2.*q)).^2)).*(2.*w.*(cosh(q)).^2+k.*c_g).^2/(g.*h-c_g.^2)))/c_g;

alpha_deep= (-w/(8*k^2))/c_g^3;
beta_deep= ((-w*k^2)/2)/c_g;

%%
 x=-16.15;
 t_lim=600;
 t=-t_lim+x/c_g:1/100:t_lim+x/c_g;
if alpha*beta<0
X=sqrt(-beta/(2*alpha))*a*(t-(1/c_g)*x);
T=sqrt(-beta/(2*alpha))^2*a^2*alpha*x;
else
X=sqrt(beta/(2*alpha))*a*(t-(1/c_g)*x);
T=sqrt(beta/(2*alpha))^2*a^2*alpha*x;
end

Q=a*exp(-0.5*i*w*a^2*k^2*t);
%% Noise
noisefrac = 50/100;
Q=a.*(1 + noisefrac*randn(size(Q)));

factor=2.4%3.3;

Amplitude = factor  * real(Q.*exp(1i.*(k.*x-w.*t)));

ende=length(Amplitude);
Amplitude=lowpass(Amplitude,1.95,100);

if Amplitude(1)>0
    t0 = find(Amplitude<0,1,'first');
    t1 = find(Amplitude<0,1,'last');
else
    t0 = find(Amplitude>0,1,'first');
    t1 = find(Amplitude>0,1,'last');
end

A2=[0 Amplitude(t0:t1) 0]';
if t1==length(t)
    t(t1+1)=t(t1);
end

t2=[ t(t0-1:t1+1) ];
plot(t2,A2,'b')
csvwrite('run_001.txt',[t2'-min(t2),A2])

%%
A3=lowpass(A2,1.95,100);

fft_part =ifft(A3); 

L=length(fft_part);

fa=100;
f = fa*(0:(L/2))/L;

P2 = abs(fft_part/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);

for j=1:L/2
   if f(j)>1.97
       P1(j)=0;
   end
end
figure()
l1=plot(f,P1);
set(l1,'LineWidth',.9)
xlim([0,4])
figure()
hold on
plot(t2,A2)
plot(t2,A3)
hold off

csvwrite('run_001.txt',[t2'-min(t2),A3])

