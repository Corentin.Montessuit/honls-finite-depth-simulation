function [h,k,sigma,cg,cp,lambda,nu,mu,muintexp] = depthdependent_NLSparameters1(omega,kh,x)
    %%
    % input params 
    % omega is the angular frequency in units of g^{1/2}
    % hk is the current value of h(x)k(x)
    % x is the position along the propagation distance
    % 
    % output params
    % h(x) depth
    % k(x) wavenumber
    % sigma = tanh(kh)
    % cg = group velocity
    % cp = phase velocity
    % lambda(x), mu(x) are the GVD and nonlinear coefficients, see
    % Djordjevic&Redekopp ZAMP 1978
    % muintexp is the exp of the integral of nu(x) exp(int(mu))
    %
    % Andrea ARMAROLI, GAP UniGE, 15/01/2019
    %%
    
    % auxiliary quantities
    sigma = tanh(kh);
    k = omega.^2./sigma;
    h = kh.*sigma./omega^2;
    % group and phase speed
    cp = omega./k;
    cg = (sigma + kh.*(1-sigma.^2))/2./omega;
    
    % NLS quantities
    lambda = -(1 - h./cg.^2.*(1-kh.*sigma).*(1-sigma.^2))./(2*omega.*cg);
    nu = k.^2.*omega./(16*sigma.^4.*cg).*(9 - 10*sigma.^2 + 9*sigma.^4-...
        2.*sigma.^2.*cg.^2./(h-cg.^2).*(4*cp.^2./cg.^2 + 4* cp./cg.*(1-sigma.^2) + h./cg.^2.*(1-sigma.^2).^2));
%     nu = 
    
    % shoaling (depth-related loss/amplification)
    mu = (1-sigma.^2).*(1-kh.*sigma)./(sigma+kh.*(1-sigma.^2));
    muintexp = ((2.*kh + sinh(2*kh))./cosh(kh).^2).^0.5;
%     muintexp = cg.^.5;
    
end