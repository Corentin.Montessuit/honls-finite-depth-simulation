% function k=resolwkh(h,T);
function k=resolwkh(h,T);
omega=2*pi/T;
g=9.81; ka=omega^2/g; kb=10; prec=1e-10;
y1=omega.^2-g.*ka.*tanh(ka.*h); y2=omega.^2-g.*kb;
    while abs(kb-ka)>prec                  
       k=ka-y1*(kb-ka)/(y2-y1);  
       y3=omega.^2-g.*k.*tanh(k.*h);
       ka=kb; y1=y2; kb=k; y2=y3;             
     end;
