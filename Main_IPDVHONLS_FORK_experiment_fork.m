%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Variable depth NLS extended to 4th order
% See Djordjevic & Redekopp ZAMP 1978
% For the HO coefficients, see Sedletsky JETP 2003 (and same author works up to today)
%
% i B_X = -lambda B_TT + nu |B|^2B - i mu B + i (-alpha B_TTT + beta |A|^2
% A_T + gamma A^2 A^*_T) + alpha4 B_TTTT
% all coefficients depend on X in a cumbersome way, see References above 
% (Sedletsky writes the time evolution, here we employ the space evolution, coefficients change: at 4th order they involve 3rd order)
% The small parameter epsilon does NOT appear explicitely
%
% The reasonable normalization is to put g = 1, so that omega (FIXED!!!) is in unit of
% [m^-1/2] and set kh as a function of X (this IS the important parameter to discern focusing/defocusing regimes).
% Now omega^2 = k tanh kh = k sigma. We can derive h or k explicitely and
% find the dependence of cg, lambda, nu, mu. 
% omega = 1 could as well be chosen. Any different omega = pippo corresponds to a
% rescaling of time (multiply by pippo^-1) and lengths (divide by pippo^2)
%
% The solution is made by means of integrating factor or interaction
% picture. 
%
% The place of the shoaling term (mu B) is customary: the linear step is a better choice, since its integration is simpler (it is a log derivative!!!) 
% Only the 3O shoaling is included for now. The 4O is more cumbersome, but
% can be derived by simple energy arguments. 
%
% Andrea ARMAROLI, GAP University of Geneva, 26.02.2019
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clear all;
% acceleration of gravity(used only in plotting)
g0 = 9.81;
crit= 1.363;
DimensionOptions= {'Dimensional','Adimentional'}; 
defSelection = DimensionOptions{2};
Dimensionflag = bttnChoiseDialog(DimensionOptions, 'Input Style', defSelection,...
 'Input Style'); 
fprintf( 'User selection "%s"\n',DimensionOptions{Dimensionflag});

% selecting a function for kh 
switch Dimensionflag
     case 1 
        prompt = {'\Omega [1/s] Real freq.','L_\xi [m]','L_\tau (time window, periods/soliton length) [ad]',...
            'n_t (log_2)','Number of steps to store N_x','\epsilon (steepness = k_0 a_0) [ad]',...
            'Shoaling ON/OFF','FOD ON/OFF','Export initial function','W-M Factor','Disipation','Output time [s]'};
        dlg_title = 'Parameters';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1.01','10','120','12','100','0.06','1','1','no','1.83','0.00','100'};
        params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        % normalized frequency omega = omega[s^-1]/g^{1/2}
        Omega_dim = str2double(params(1));
        Omega = Omega_dim*(2*pi)/sqrt(g0);
        T0_dim=1/Omega_dim;
        fprintf('Dimentional frequency: %.3f \n , Adimentional frequency: %.3f \n ', Omega_dim, Omega)
        Lx = str2double(params(2));
        Nper = str2double(params(3));
        nt = 2.^str2double(params(4));
        % number of points to save
        Nx = str2double(params(5));
        esteep0 = str2double(params(6));
        shoalingflag = str2double(params(7));
        FODflag = str2double(params(8)); 
        Exportinit =  string(params(9));
        Factor=str2double(params(10));
        dis=str2double(params(11));
        Out_time=str2double(params(12));

     case 2
        prompt = {'\Omega [m^{-1/2}]','L_\xi [m]','L_\tau (time window, periods/soliton length) [ad]',...
            'n_t (log_2)','Number of steps to store N_x','\epsilon (steepness = k_0 a_0) [ad]',...
            'Shoaling ON/OFF','FOD ON/OFF','Export initial function','W-M Factor','Disipation'};
        dlg_title = 'Parameters';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1','50','10','11','100','0.14','1','0','yes','1','0.0021072'};
        params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        % normalized frequency omega = omega[s^-1]/g^{1/2}
        Omega = str2double(params(1));
        Omega_dim = Omega*sqrt(g0)/(2*pi);
            T0_dim=1/Omega_dim;
        fprintf('Dimentional frequency: %.3f \n , Adimentional frequency: %.3f \n ', Omega_dim, Omega)
        Lx = str2double(params(2));
        Nper = str2double(params(3));
        nt = 2.^str2double(params(4));
        % number of points to save
        Nx = str2double(params(5));
        esteep0 = str2double(params(6));
        
        shoalingflag = str2double(params(7));
        FODflag = str2double(params(8));
        Exportinit =  string(params(9));
        Factor=str2double(params(10));
        dis=str2double(params(11));

end        

%% Bathymetry
% hk is expressed as a function hk = 1.363 is the focusing/defocusing
% turning poin
bathymetryOptions = {'Linear A->B','Erfc A->B','Linear A->B->A','Erfc A->B->A'}; 
defSelection = bathymetryOptions{2};
bathymetryflag = bttnChoiseDialog(bathymetryOptions, 'Bathymetry profile', defSelection,...
 'Bathymetry profile'); 
fprintf( 'User selection "%s"\n', bathymetryOptions{bathymetryflag});

% selecting a function for kh 
switch bathymetryflag
    case 1
        khfunsel =  @khfun4;
    case 2
        khfunsel =  @khfun1;
    case 3
        khfunsel =  @khfun3;
    case 4
        khfunsel =  @khfun2;
end


% bathymetry params
% hk = 1.363 is the focusing/defocusing turning point

prompt = {'hk_{0}','hk_{fin}','\xi_1','\xi_2','\xi_{slope}'};
dlg_title = 'Solver parameters';
options.Interpreter = 'tex';
num_lines = 1;
defaultans = {'1.05','1.05','13.5','15.5','20'};
bathymetrypar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
kh0 = str2double(bathymetrypar(1));
khfin = str2double(bathymetrypar(2));
xvarstart = str2double(bathymetrypar(3)); %[m]
xvarstop = str2double(bathymetrypar(4)); %[m]
xslope = str2double(bathymetrypar(5));

if bathymetryflag == 1
    warning('Slope is not used!');
end

% hk is expressed as a function hk = 1.363 is the focusing/defocusing
% turning point
% kh0 = 2.5;
% khfin = 3.45;
% xvarstart = 480; %[m]
% xvarstop = 520; %[m]
% xslope = 10;

% selecting a function for kh
% khfunsel =  @khfun4;

%% solver params;
prompt = {'initial step','minimum step','tolerance'};
dlg_title = 'Solver parameters';
options.Interpreter = 'tex';
num_lines = 1;
defaultans = {'0.01','1e-6','1e-9'};
solvpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
hx0 = str2double(solvpar(1));
hxmin = str2double(solvpar(2));
tol = str2double(solvpar(3));
% Lx = 1000; % [m]
% number of points to save
% Nx = 250;
% hx0 = 0.01;
% hxmin = 1e-6;
% tol = 1e-10;

%% definition of X lattice
if hx0<Lx./Nx
    hx0 = Lx./Nx;
end
Hx = Lx./Nx;
X = 0:Hx:Lx;
% nt = 2^12;
% Nper = 20;

%% compute the NLS parameters 
% DO NOT DISPLACE IT, it is necessary to estimate MI, soliton, DSW params
[H,K,Sigma,Cg,Cp,Lambda,Nu,Alpha,Alpha4,Beta,Gamma,Mu,Muintexp] = depthdependent_HONLSparameters1(Omega,khfunsel(X,kh0,khfin,xvarstart,xvarstop,xslope),X,FODflag);
figure;
subplot(221);
plot(X,H,X,Mu,'LineWidth',1.5);
% title('Pause, press any key to continue...','FontSize',20)
legend({'h [m]','\mu [m^{-1}]'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);
subplot(422);
plot(X,K,X,Cp./Cg,X(2:end),Muintexp(2:end)./Muintexp(1:end-1),'LineWidth',1.5);
legend({'k [m^{-1}]','c_p/c_g','exp(\int \mu)'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);
subplot(223);
plot(X,Nu./Lambda,'LineWidth',1.5);
legend({'\nu/ \lambda'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);
subplot(224);
plot(X,Alpha,X,Alpha4,X,Beta,X,Gamma,'LineWidth',1.5);
legend({'\alpha (TOD)','\alpha_4 (FOT)','\beta','\gamma'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);

% ...and their initial values
[h0,k0,sigma0,cg0,cp0,lambda0,nu0,alpha0,alpha40,beta0,gamma0,mu0,muintexp0] = depthdependent_HONLSparameters1(Omega,kh0,0,FODflag);
% Reference amplitude
B0 = esteep0./k0;
% Normalization scales
Tnl = sqrt(abs(2*lambda0./nu0))./B0;
Lnl = 1./abs(nu0)/B0^2;

%% excitation type
inputOptions = {'Shot noise like perturbed plane wave','Harmonically perturbed plane wave','Akhmediev Breather',...
    'Peregrine Breather','Bright Soliton','Dark Soliton','Gaussian on top of a background',...
    'Sedletsky bright soliton','Steplike initial condition (dam break)','Amin noise','Benchmark Gaussian'}; 
defSelection = inputOptions{2};
excflag = bttnChoiseDialog(inputOptions, 'Type of initial conditions', defSelection,...
 'Type of initial conditions'); 
fprintf( 'User selection "%s"\n', inputOptions{excflag});

% At each type of initial condition, different parameters are associated.
% Moveover, omega0 (on which the time window depends) and the plotting
% limit windowwidth are different.
switch excflag
    case 1
        if nu0./lambda0 > 0
            warning('Plane waves are stable');
        end
        prompt = {sprintf('Noise fraction, B_0 = %g',B0)};
        dlg_title = 'Noisy Plane Wave';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1e-4'};        
        NPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=NPWpar;

        etanoise =  str2double( NPWpar(1));
        eta0 = 1-etanoise;
        U0 = str2double( NPWpar(1));
        variance = etanoise/nt*2/pi;
        omega0 = 1/Tnl*sqrt(2);
        windowwidth = 10*Tnl;
        
    case 2
        if nu0./lambda0 > 0
            warning('Plane waves are stable');
        end
        prompt = {'Total initial energy','Sideband fraction','Sideband unbalance','Relative phase (\phi_1+\phi_{-1})/2-\phi_0','\Omega/\Omega_Max'};
        dlg_title = 'Harmonically perturbed Plane Wave';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1','1e-2','0e-3','pi/2','1'};
        
        PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=PPWpar;

        U0 = str2double( PPWpar(1));
        eta0 = U0 - str2double(PPWpar(2));
        alpha = str2double(PPWpar(3));
        psi0 = (eval(char(PPWpar(4))));
        omega0 = 1/Tnl*sqrt(2)*str2double(PPWpar(5));
        windowwidth = 10*Tnl;
        eta1 = (U0-eta0+alpha)/2;
        etam1 = (U0-eta0-alpha)/2;
        
        if eta1*etam1 < 0
            error('The chosen unbalance is to big or negative intensity are chosen!');
        end
        

    case 3
        if nu0./lambda0 > 0
            warning('Plane waves are stable');
        end
        prompt = {'a','Initial point \xi_0'};
        dlg_title = 'Akhmediev Breather';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'0.25','-500'};
        
        ABpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=ABpar;

        aAB = str2double( ABpar(1));
        X0 = str2double( ABpar(2));
        bAB = sqrt(8*aAB*(1-2*aAB));
        omegaAB = 2*sqrt(1-2*aAB);
        omega0 = omegaAB/Tnl;
        windowwidth = 10*Tnl;
        
    case 4
        if nu0./lambda0 > 0
            warning('Plane waves are stable');
        end
        prompt = {'Initial point \xi_0'};
        dlg_title = 'Peregrine Breather';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'-500'};
        
        Perpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);          
        save_params=Perpar;

        X0 = str2double( Perpar(1));
        
        omega0 = 2*pi/Tnl;
        windowwidth = 20*Tnl;
        if Nper<20
            warning('The time window is too narrow!')
        end
        
        
    case 5
        if nu0./lambda0 > 0
            warning('Bright soliton is not a solution');
        end
        prompt = {'Soliton width','Soliton number'};
        dlg_title = 'N-soliton';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1','1'};
        
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Solpar;

        tau0 = str2double(Solpar(1));
        Nsol = str2double( Solpar(2));
       
        omega0 = 2*pi/tau0/Tnl;
        windowwidth = 20*Tnl;
        if Nper<20
            warning('The time window is too narrow!')
        end

     case 6
        if nu0./lambda0 < 0
            warning('Dark soliton is not a solution');
        end
        prompt = {'Soliton width','Soliton number'};
        dlg_title = 'N-soliton';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1','1'};
        
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Solpar;

        tau0 = str2double(Solpar(1));        
        Nsol = str2double( Solpar(2));
       
        omega0 = 2*pi/tau0/Tnl;
        windowwidth = 20*Tnl;
        
        if Nper<50
           warning('The time window is too narrow!')
        end
    case 7
        if nu0./lambda0 < 0
            warning('The dynamics maybe unstable in the defocusing regime!');
        end
        if Nper<50
           warning('The time window is too narrow!')
        end
        prompt = {'Perturbation width (in Tnl units)','Background rel. amplitude','Perturbation rel. amplitude','Supergaussian number (=1 Gaussian)'};
        dlg_title = 'N-soliton';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1','1','5','1'};
        
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
                save_params=Pertpar;

        tau0 = str2double(Pertpar(1));           
        Bback = str2double( Pertpar(2));
        Bpert = str2double( Pertpar(3));
        MSG = str2double( Pertpar(4));
        
        omega0 = 2*pi/tau0/Tnl;
        windowwidth = 10*Tnl;
        
    case 8
        if nu0./lambda0 < 0
            warning('This is a special bright soliton');
        end
        prompt = {'t_0'};
        dlg_title = 'Sedletsky 1-soliton';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'-200'};
        
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
                save_params=Solpar;

        t0 = str2double(Solpar(1));        
     
       
        omega0 = 2*pi/Tnl;
        windowwidth = 60*Tnl;
        
        if Nper<50
           warning('The time window is too narrow!')
        end
        
      case 9
              
        % Steplike profile (dam break)
        if nu0./lambda0 < 0
            warning('The dynamics maybe unstable in the defocusing regime!');
        end
        if Nper<50
           warning('The time window is too narrow!')
        end
        prompt = {'Perturbation offset (in Tnl units)','Background rel. amplitude (in units of \varepsilon)','Perturbation rel. amplitude','Supergaussian number (=1 Gaussian)'};
        dlg_title = 'Steplike for dam break';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'0','1','5','100'};
        
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;

        toff = str2double(Pertpar(1));           
        Bback = str2double( Pertpar(2));
        Bpert = str2double( Pertpar(3));
        MSG = str2double( Pertpar(4));
        
        omega0 = 2*pi/Tnl;
        windowwidth = 50*Tnl;
		
		
		   case 10
              
        % Real noise pert plane wave
        if nu0./lambda0 < 0
            warning('The dynamics maybe unstable in the defocusing regime!');
        end
        if Nper<50
           warning('The time window is too narrow!')
        end
        prompt = {'Noise fraction'};
        dlg_title = 'Noisy plane wave perturbated only on the amplitude';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'0.01'};
        
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;

        frac = str2double(Pertpar(1));                 
        omega0 = 2*pi/Tnl;
        windowwidth = 50*Tnl;
		
	case 11
        % Super-gaussian on a background 
        if nu0./lambda0 < 0
            warning('The dynamics maybe unstable in the defocusing regime!');
        end
        if Nper<50
           warning('The time window is too narrow!')
        end
        prompt = {'m'};
        dlg_title = 'N-soliton';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'4'};
        
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;

        m_gaus = str2double(Pertpar(1));           
     omega0=Omega;
		
        windowwidth = 20*Tnl;
        
    otherwise 
        error('No valid initial condition option');
end
save_params_names=prompt;%TO BE USED TO SAVE PARAMETERS TO FILE. 

%%
% % MI excitation parameters
% eta0 = 1-1e-1;
% alpha = .04;
% eta1 = (1-eta0+alpha)/2;
% etam1 = (1-eta0-alpha)/2;
% if eta1*etam1 < 0
%     error('The chosen unbalance is to big or negative intensity are chosen!');
% end
% psi0 = 0* pi/2;

%% definition of T lattice
% given the type of IC, we define omega0 and the time lattice
% omega0 is the characteristic  angular frequency dependent on the chosen initial
% conditions

% omega0 = sqrt(abs(nu0/lambda0)).*B0;
% omega0 = 0.1;
T0 = 2*pi/omega0;
% calculate the time window span
Lt = T0*Nper;
t = linspace(-Lt/2,Lt/2,nt+1);
t = t(1:end-1);
ht = t(2)-t(1);
omegaxis = 2*pi.*linspace(-1/ht/2,1/ht/2,nt+1); omegaxis = omegaxis(1:end-1);
%FFT phase shifts
kt1 = linspace(0,nt/2 + 1,nt/2)';
kt2 = linspace(-nt/2,-1,nt/2)';
kt = ((2*pi/ht/nt)*[kt1; kt2]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% field initialization
u = zeros(nt,Nx+1);
uspectrum = u;
saveflag = zeros (Nx+1,1);
N = saveflag;
P = N;

%% Initial conditions
switch excflag
    case 1
        % MI from noise
        % variance = 1e-2;
        u0 = B0.*(sqrt(eta0) + sqrt(variance).*randn(size(t)) + 1i.*sqrt(variance).*randn(size(t)));

    case 2
        % Harmonically perturbed plane wave
        u0 = B0.*(sqrt(eta0) + sqrt(etam1).*exp(1i*omega0.*t).*exp(1i.*psi0) + sqrt(eta1).*exp(-1i*omega0.*t).*exp(1i.*psi0)).*exp(-0*t.^2./(30*T0.^2));
    
    case 3
        % Akhmediev Breather
%         aAB = 0.1;
%         bAB = sqrt(8*aAB*(1-2*aAB));
%         omegaAB = 2*sqrt(1-2*aAB);
        u0 = B0.*(1 + (omegaAB^2/2.*cosh(bAB*X0/Lnl) - 1i*bAB*sinh(bAB.*X0/Lnl))./(sqrt(2*aAB)*cos(omegaAB*t/Tnl) - cosh(bAB.*X0/Lnl))).*exp(1i*X0/Lnl);    
    
    case 4
        % Peregrine Soliton
        % TP = sqrt(abs(2*lambda0./nu0))./B0;
        % Lnl = 1./nu0/B0^2;
        % X0 = -500;
        u0 = B0.*(1-4.*(1-2i.*X0./Lnl)./(1+4*(t./Tnl).^2 + 4*(X0/Lnl)^2)).*exp(-1i*X0/Lnl);
    case 5
        % Bright Soliton
        % soliton order
        % Nsol = 3;
        u0 = Nsol*(B0/tau0).*sech((B0/tau0)*sqrt(abs(nu0/2/lambda0)).*t);
        
    case 6
        % Dark Soliton
        u0 = Nsol*B0*tanh(B0*sqrt(abs(nu0/2/lambda0)).*t);
    
    case 7
        % Gaussian on top of a background 
        u0 = B0*(Bback + Bpert*exp(-(t./Tnl/tau0).^(2*MSG)));
                
    case 8
        % Bright soliton in defocusing regime, see Gandzha&Sedletsky PLA
        % 381 (2017)
        S = -(beta0+gamma0)./(6*alpha0);
        K0 = B0*sqrt(S);
        omegasol0 = (-lambda0*(beta0+gamma0) + 3*alpha0*nu0)/(6*alpha0*gamma0);        
        V = K0^2*alpha0 - 2.*lambda0*omegasol0 - 3.*alpha0*omegasol0^2;
        u0 = B0.*sech(K0.*(t-t0)).*exp(-1i*t*omegasol0);
        
    case 9
        % Dam break
        u0 = B0*(Bback + Bpert*exp(-((t+Lt/2-toff)./Lt*2).^(2*MSG))).*exp(-(t./2/Lt*5).^50);    
   
	case 10
        % Real noise perturbated plane wave
        u0 = B0.*(frac.*randn(size(t)));
		
    case 11
        % Real noise perturbated plane wave
        u0 = B0*(exp(-(t./(m_gaus.*T0)).^(2)));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialization of computation
u(:,1) = (u0);
ucurr = u(:,1);
uspectrum(:,1) = fftshift(ifft(ucurr,[],1),1);

pind = 1;
N(pind) = trapz(t,cg0*abs(ucurr).^2);
P(pind) = real(trapz(t,cg0*0.5i.*(fft(-1i.*kt.*ifft(ucurr)).*conj(ucurr)-fft(-1i.*kt.*ifft(conj(ucurr))).*ucurr)));
x = 0;
hx = hx0;
pind = 2;
count = 0;

%% Computation
figure;
filename = 'NLSAB.gif';
set(0,'defaulttextfontsize',14);
while x<Lx
        err = tol*10;
        numberofrejstep = 0;
        while err>tol
            [~,k1,~,cg1,~,lambda1,nu1,alpha1,alpha41,beta1,gamma1,~,muintexp1] = depthdependent_HONLSparameters1(Omega,khfunsel(x+hx/2,kh0,khfin,xvarstart,xvarstop,xslope),x,FODflag);
            [~,k2,~,cg2,~,lambda2,nu2,alpha2,alpha42,beta2,gamma2,~,muintexp2] = depthdependent_HONLSparameters1(Omega,khfunsel(x+hx,kh0,khfin,xvarstart,xvarstop,xslope),x,FODflag);
            [ucurrnew,err] = DV4ORK34_1(hx,ucurr,kt,lambda0,lambda1,lambda2,nu0,nu1,nu2,alpha0,alpha1,alpha2,alpha40,alpha41,alpha42,beta0,beta1,beta2,gamma0,gamma1,gamma2,muintexp0,muintexp1,muintexp2,shoalingflag,dis);           
            if (err<tol)
                x = x + hx;
                ucurr = ucurrnew;
                count = count + 1;
                step(count) = hx;
            else
                numberofrejstep =  numberofrejstep + 1;
                if numberofrejstep > 10
                    fprintf('10!\n');
                end
            end
            hx = min(max(0.5,min(2,.95*(tol./err).^.25)).*hx,Hx);
            if hx < hxmin 
                error('Integration step too small');
            end
            if isnan(err)
                error('Diverges!');
            end
        end
    if isnan(ucurr)
        error('diverges!!!');
    end
    k0 = k2;
    lambda0 = lambda2;
    muintexp0 = muintexp2;
    nu0 = nu2;
    cg0 = cg2;
    alpha0 = alpha2;
    alpha40 = alpha42;
    beta0 = beta2;
    gamma0 = gamma2;
    
    %     hz0 = hz;
    if x>=X(pind) && saveflag(pind) == 0;
        if max(abs(ucurr))*k2>0.4
            warning('Wave breaking probably occurs!');
        end
        saveflag(pind) = 1;   
        N(pind) = trapz(t,cg0*abs(ucurr).^2);
        P(pind) = real(trapz(t,cg0*0.5i.*(fft(-1i.*kt.*ifft(ucurr)).*conj(ucurr)-fft(-1i.*kt.*ifft(conj(ucurr))).*ucurr)));
%         NQ(pind) = trapz(t,(abs(ucurr).^2-1).^2);
%         fprintf('Writing the field %d, X = %g, N = %g, P = %g\n\n', pind,x, N(pind), P(pind)); 
            u(:,pind) = ucurr;
            % computing spectrum
            uspectrum(:,pind) = fftshift(ifft(ucurr,[],1),1);
            X(pind) = x;
            subplot(131)
            plot(t,abs(ucurr),t,abs(u0));
            xlim([-windowwidth,windowwidth]);
            ylim([0,5*B0]);
            xlabel('T [m^{1/2}]','fontsize',14); 
            set(gca,'fontsize',13);
            subplot(132)
            plot(t,unwrap(angle(ucurr))/pi,t,unwrap(angle(u0))/pi);
            xlabel('T [m^{1/2}]','fontsize',14);
            xlim([-windowwidth,windowwidth]);
            title(strcat('\xi = ',num2str(X(pind))));
            set(gca,'fontsize',13);        
            subplot(133)
            semilogy(omegaxis,abs(uspectrum(:,pind)).^2,omegaxis,abs(uspectrum(:,1)).^2);
            xlabel('\delta \Omega [m^{-1/2}]','fontsize',14);
            xlim([-5*sqrt(2)*omega0,5*sqrt(2)*omega0]);
            title(strcat('\xi = ',num2str(X(pind))));
            set(gca,'fontsize',13);        
            drawnow
        F = getframe(gcf);
        im = frame2im(F);
        [imind,cm] = rgb2ind(im,256);
     %   if pind == 2;
            
    %        imwrite(imind,cm,filename,'gif', 'DelayTime',.1,'Loopcount',inf);
            
     %   else
            
     %       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',.1);
            
     %   end
        
        pind = pind + 1;

    end
    
end

%% Generic plots
%% recover the old variables (see B above)
u = u.*(ones(length(t),1)*(Cg.^-0.5)).*Cg(1).^0.5;
uspectrum =  uspectrum.*(ones(length(t),1)*(Cg.^-0.5)).*Cg(1).^0.5;

tplot = abs(t)<=windowwidth;
%% export initial conditions for experiment on sydney
if Exportinit=='yes'
    sample_frequency = 100. ;%hz this is the sample frequency of the output. 
    initial_depth= H(1);
    pos_gau=import_pos_gau_for_input('pos_wavegauges.txt');
    pos_gau=table2array(pos_gau);
    %% make header
    Input_function=inputOptions{excflag};%manually choose the input function to be used
    Posible_functions=['Mod_instability','Soliton','Ak_Breather','Peregrine','Super_Gaussian'];
    input_signal=Factor*real(u(tplot,1).*exp(1i*(Omega_dim*(2*pi)*t(tplot)/sqrt(g0)))');
    
    figure('name','Output sample');
    tp=t(tplot)/sqrt(g0);
    [A1,t_sample] = resample(input_signal,tp,100);
    fprintf('sanity check: Wave maker sample frequency shoud be 0.01 \n sample frequency is:  %.3f \n ',t_sample(2)-t_sample(1))
    if A1(1)>0
        t0 = find(A1<0,1,'first');
        t1 = find(A1<0,1,'last');
    else
        t0 = find(A1>0,1,'first');
        t1 = find(A1>0,1,'last');
    end
    %borders at 0 (a tukey window might work also.)
    A2=[0 A1(t0:t1)' 0]';
    if t1==length(t_sample)
        t_sample(t1+1)=t_sample(t1);
    end
    t2= t_sample(t0-1:t1+1) ;
    M=[t2'-min(t2),A2];
    %% saving stuff
    [flm fullfolder]=save_filename(Omega_dim,B0,esteep0,T0_dim);
    mkdir(fullfolder) 
    filename_input=strcat(fullfolder,'\',flm,'_input.txt');
    print_input_file2(filename_input,Input_function,sample_frequency,initial_depth,Factor,M)
    filename_wm=strcat(fullfolder,'\',flm,'_run_001.txt');
    csvwrite(filename_wm,M) ;
    filename_pos=strcat(fullfolder,'\',flm,'_pos.txt');
    print_pos_file(filename_pos,Input_function,sample_frequency,initial_depth,Factor,pos_gau)
    mkdir(strcat(fullfolder,'\','simulation and input')) 
    img_folder=strcat(fullfolder,'\','simulation and input');
    filename_params=strcat(fullfolder,'\',flm,'_params.txt');
    print_params_file(filename_params,Input_function,sample_frequency, save_params,save_params_names)
    figure('name','signal')
    title('Translated in time and borders at 0','FontSize',15)
    plot(t2'-min(t2),A2,'b');
    xlabel('T (s)','fontsize',14);
    ylabel('Amplitude (m)','fontsize',14);
    savefig(strcat(img_folder,'\',flm,'_input_signal'))
    
    figure('name','Resample')
    title('Resample from simulation','FontSize',15)
    hold on
    plot(t_sample,A1,'.r')
    plot(tp,input_signal,'-b')
    hold off
    xlabel('T (s)','fontsize',14);
    ylabel('Amplitude (m)','fontsize',14);
    savefig(strcat(img_folder,'\',flm,'_sample_check'))

        figure('name','signal')
    title('Translated in time and borders at 0','FontSize',15)
    subplot(211);
    l1=plot(t2'-min(t2),A2,'b');
    set(l1,'LineWidth',.8)
    xlabel('T (s)','fontsize',14);
    ylabel('Amplitude (m)','fontsize',14);
    subplot(212);
    sig_ff=ifft(A2);
    L=length(sig_ff);
    P2 = abs(sig_ff/L);
    P1 = P2(1:L/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    fa=100;
%Define the frequency domain f and plot the single-sided amplitude spectrum P1. The amplitudes are not exactly at 0.7 and 1, as expected, because of the added noise.
% On average, longer signals produce better frequency approximations.
    f = fa*(0:(L/2))/L;
    pl_1=plot(f,P1);
    set(pl_1,'LineWidth',1)
    xlim([0,3]); 
    savefig(strcat(img_folder,'\',flm,'_input_signal'))
end

%% Generic plots
figure('name','Params');
subplot(211);
plot(X,Cg,X,Lambda,'LineWidth',1.5);
% title('Pause, press any key to continue...','FontSize',20)
%legend({'h [m]','\mu [m^{-1}]'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);
subplot(212);
plot(X,Nu,X,Nu./Lambda.*cg0./Cg,'LineWidth',1.5);
%legend({'\nu/ \lambda'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);
if Exportinit=='yes'
    savefig(strcat(img_folder,'\',flm,'_params'))
end 
%%
figure('name','Conservations');
subplot(311);
plot(X,N,'linewidth',1.5);
set(gca,'FontSize',14);
ylabel('N/N(0)','fontsize',16);
subplot(312);
plot(X,P,'linewidth',1.5);
set(gca,'FontSize',14);
xlabel('X','fontsize',16);
ylabel('P/P(0)','fontsize',16);
subplot(313);
plot(X(2:end),diff(real(P./N)),'linewidth',1.5);
set(gca,'FontSize',14);
xlabel('X','fontsize',16);
ylabel('d(N/N0)/dx','fontsize',16);
% suptitle('Conservation of momenta');

%% Data processing: extracting sidebands for three-wave phase-space
IAS = find(omegaxis<=omega0+.1 & omegaxis>=omega0-.1); IAS = IAS(round(length(IAS)/2));
IS = find(omegaxis<=-omega0+.1 & omegaxis>=-omega0-.1); IS = IS(round(length(IS)/2));
IAS2 = find(omegaxis<=2*omega0+.1 & omegaxis>=2*omega0-.1); IAS2 = IAS2(round(length(IAS2)/2));
IS2 = find(omegaxis<=-2*omega0+.1 & omegaxis>=-2*omega0-.1); IS2 = IS2(round(length(IS2)/2));
IAS3 = find(omegaxis<=3*omega0+.1 & omegaxis>=3*omega0-.1); IAS3 = IAS3(round(length(IAS2)/2));
IS3 = find(omegaxis<=-3*omega0+.1 & omegaxis>=-3*omega0-.1); IS3 = IS3(round(length(IS2)/2));

utrunc = uspectrum([nt/2+1,IS,IAS,IS2,IAS2,IS3,IAS3],:);
Eta0 = abs(utrunc(1,:)).^2/B0^2;
Eta1 = abs(utrunc(3,:)).^2/B0^2;
Etam1 = abs(utrunc(2,:)).^2/B0^2;
Eta2 = abs(utrunc([5],:)).^2/B0^2;
Etam2 = abs(utrunc([4],:)).^2/B0^2;
Alpha = Eta1 - Etam1;
Utr = Eta0 + Eta1 + Etam1;
Psi = unwrap(angle(utrunc(3,:)))+unwrap(angle(utrunc(2,:))) - 2*unwrap(angle(utrunc(1,:)));
%%
figure('name','regime and decomp');
axes('Position',[.12,.58,.74, .36]);
plot(X, Eta0, X, Eta1,'g-.', X, Etam1,'r-.', ...
    X, Eta2,'g--', X , Etam2,'r--', X, Utr,'k:','linewidth',1.2);
% legend('\eta_0','\eta_1','\eta_{-1}','\eta_{-2}','\eta_{-3}','\eta_{2}','\eta+\eta_1+\eta_{-1}');
xlim([0,Lx]);
set(gca,'FontSize',14,'XTickLabel','');
ylabel('\eta_n','FontSize',15)
% text(.15,.8,'(a)');

axes('Position',[.12,.15,.74, .36]);
[phax,phlin,nl] = plotyy(X, Psi/2/pi, ...
    X, omega0./sqrt(abs(Nu./Lambda))./B0);
set(phlin,'linewidth',1.5,'color','b');
set(nl,'linewidth',1.5,'linestyle','-','color','r');
hold(phax(1),'on');
% plot(phax(2),X,sqrt(2).*ones(size(X)),':','linewidth',1.2)
plot(phax(1),X,ones(size(X)),':b','linewidth',1.2)
% legend('\Omega\alpha','P/N');
xlabel('X','fontsize',15);
xlim([0,Lx]); 
% ylim([-.5,.5]);grid;
set(phax(1),'FontSize',14,'xlim',[0,Lx],'Ycolor','b');
set(phax(2),'FontSize',14,'xlim',[0,Lx],'Ycolor','r');
ylabel(phax(1),'\psi/\pi','FontSize',15);
ylabel(phax(2),'\nu/\lambda','FontSize',15);
% text(.15,1.35,'(b)');
hold(phax(1),'off');
if Exportinit=='yes'
   savefig(strcat(img_folder,'\',flm,'_regime'))
end

%phase plane plots
figure('name','3w space 1');
plot(Psi/2/pi,(Eta1+Etam1)./ Utr,Psi(1)/2/pi,(Eta1(1)+Etam1(1))./Utr(1),'rx',Psi/2/pi,(Eta0)./ Utr,'--',Psi(1)/2/pi,Eta0(1)./Utr(1),'rx','linewidth',1);
set(gca,'FontSize',14);
xlabel('\psi/\pi','Fontsize',16);
ylabel('\eta','Fontsize',16);
if Exportinit=='yes'
    savefig(strcat(img_folder,'\',flm,'_3W_space1'))
end 

figure('name','3w space 2');
plot((Eta1+Etam1)./ Utr.*cos(Psi/2),(Eta1+Etam1)./ Utr.*sin(Psi/2),(Eta1(1)+Etam1(1))/Utr(1)*cos(Psi(1)/2),(Eta1(1)+Etam1(1))/Utr(1)*sin(Psi(1)/2),'rx','linewidth',1);
set(gca,'FontSize',14);
xlabel('\eta cos(\psi)','Fontsize',16);
ylabel('\eta sin(\psi)','Fontsize',16);
if Exportinit=='yes'
    savefig(strcat(img_folder,'\',flm,'_3W_space2'))
end 

% Akhmedievstyle phaseplane plot (full NLS minus global phase)
Ucomplexplane = u/B0.*exp(-1i*ones(length(t),1)*unwrap(angle(utrunc(1,:))));
figure('name','Akhmedievstyle phaseplane');
plot(real(Ucomplexplane(nt/2+1,:)),imag(Ucomplexplane(nt/2+1,:)),real(Ucomplexplane(nt/2+1,1)),imag(Ucomplexplane(nt/2+1,1)),'rx','linewidth',1);
set(gca,'FontSize',14);
xlabel('Re B(t=0)','Fontsize',16);
ylabel('Im B(t=0)','Fontsize',16);


switch Dimensionflag
    case 1    
      %%
%     figure;
%     [ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
%     hold on
%     imagesc(t(tplot)/sqrt(g0),X,abs(u(tplot,:).'));
%     set(gca,'YDir','normal','FontSize',14);
%     xlabel('\tau [s] ','fontsize',16);
%     ylabel('\xi [m]','fontsize',16);
%     % if IC=='Peregrine Soliton'
%     %     caxis([0 B0*3]);
%     % end
%     shading interp;
%     colorbar
%     % colormap jet;
%     %colormap hot;
% %     plot(t(tplot)/sqrt(g0),-X0.*ones(max(size(t(tplot)/sqrt(g0)))),'--g')
%     plot(t(tplot)/sqrt(g0),xvarstart.*ones(max(size(t(tplot)/sqrt(g0)))),'color',[0.87,0.74,0.53],'LineStyle','--')
%     plot(t(tplot)/sqrt(g0),xvarstop.*ones(max(size(t(tplot)/sqrt(g0)))),'color',[0.87,0.74,0.53],'LineStyle','--')
%     if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
%         plot(t(tplot)/sqrt(g0),X(ix).*ones(max(size(t(tplot)/sqrt(g0)))),'r','LineStyle',':')
%     end
%     axis([min(t(tplot)/sqrt(g0)) max(t(tplot)/sqrt(g0))  0 Lx])
%     hold off
%     ax1 = gca; % current axes
%     ax1_pos = ax1.Position; % position of first axes
%     ax2 = axes('Position',ax1_pos,...
%         'XAxisLocation','top',...
%         'YAxisLocation','right',...
%         'Color','none');
%     line(H.*K,X,'Parent',ax2,'Color',[0.87,0.74,0.53],'LineWidth',1.2)
%     ax2.YLim=([0. Lx]);
%     set(gca,'xlim',[0 5*max(H.*K)],'ytick',[] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})
% 
%     
%     f=figure('position',[20 20 500 500]);
%     hold on
%     UdBth = -60;
%     Usp = 20*log10(abs(uspectrum.')*sqrt(g0)*(1/(2*pi))/B0);
%     Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
%     imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,Usp);
%     set(gca,'YDir','normal','FontSize',14);
%     xlim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);
%     ylim([min(X),max(X)]);
%     xlabel('f [Hz]','fontsize',16);
%     ylabel('X [m]','fontsize',16);
%     colorbar;
% %     plot(omegaxis*sqrt(g0)/(2*pi),-X0.*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'--g')
%     plot(omegaxis*sqrt(g0)/(2*pi),xvarstart.*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'color',[0.87,0.74,0.53],'LineStyle','--')
%     plot(omegaxis*sqrt(g0)/(2*pi),xvarstop.*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'color',[0.87,0.74,0.53],'LineStyle','--')
%     if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
%         plot(omegaxis*sqrt(g0)/(2*pi),X(ix).*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'r','LineStyle',':')
%     end
%     axis([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi) 4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)  0 Lx])
%     hold off
%     ax1 = gca; % current axes
%     ax1_pos = ax1.Position; % position of first axes
%     ax2 = axes('Position',ax1_pos,...
%         'XAxisLocation','top',...
%         'YAxisLocation','right',...
%         'Color','none');
%     line(H.*K,X,'Parent',ax2,'Color',[0.87,0.74,0.53],'LineWidth',1.2)
%     ax2.YLim=([0. Lx]);
%     set(gca,'xlim',[0 5*max(H.*K)],'ytick',[] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})

    
    %%
    f=figure('name','Amplitude evolution','position',[20 20 550 550]);
    [ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
    pos1 = [0.05 0.3 0.9 0.65];
    subplot(4,1,[1 3]);
    % hold on 
    imagesc(X,t(tplot)/sqrt(g0),abs(u(tplot,:)) );
     set(gca,'XDir','normal','FontSize',14);
     ylabel('\tau [s] ','fontsize',16);
     xlabel('x [m]','fontsize',16);
    % hold off
    shading interp;
	colormap('jet')
    hc=colorbar;
    title(hc,'|B| [m]')
    ax1 = gca;
    ax1_pos = ax1.Position ;
    subplot(4,1,4);
    ax2 = gca;
    ax2_pos = ax2.Position;
    ax2_pos =  [ ax2_pos(1)  .5*ax2_pos(2)  .84*ax1_pos(3)  .7*ax2_pos(4)];
    set(ax2,'Position',ax2_pos);
    yline=H.*K;
    yline(end) = NaN;
    c = yline;
    patch(X,yline,c,'EdgeColor','interp','LineWidth',1.2,'MarkerFaceColor','flat');  
    ylabel('hk','fontsize',15);
    set(gca,'FontSize',15,'Ylim',[0.85*min(H.*K), 1.15*max(H.*K)],'ytick',[min(H.*K)  max(H.*K)*1.0000001],'XLim',[0. Lx],'xtick',[xvarstart,xvarstop]);
    ax2.XAxis.FontSize = 8;
    ylabel('hk ','fontsize',15);
    ytickformat('%.1f');
    xtickformat('%.1f');
    linkaxes([ax1, ax2], 'x');
    yyaxis(ax2, 'right');
    plot(X,-H,'--');
    y2fontsize=12;
    ylabel('h [m]','fontsize',y2fontsize);
    try
    ax2.YAxis(2).TickValues=sort([min(-H),max(-H)]);
    ax2.YAxis(2).FontSize = y2fontsize;
    ax2.YAxis(2).Exponent=-2;
    end   
    ytickformat('%.1f');
   if Exportinit=='yes'
       savefig(strcat(img_folder,'\',flm,'_2d_evol'))
       saveas(gcf,strcat(fullfolder,'\',flm,'_2d_evol','.jpeg'))
   end

    %%
    f=figure('name','Fourier','position',[20 20 500 500]);
    pos1 = [0.05 0.3 0.9 0.65];
    subplot(4,1,[1 3]);
    hold on
    UdBth = -60;
    Usp = 20*log10(abs(uspectrum.')*sqrt(g0)*(1/(2*pi))/B0);
    Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
    imagesc(X,omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),Usp');
    set(gca,'XDir','normal','FontSize',14);
    ylim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);
    xlim([min(X),max(X)]);
    ylabel('f [Hz]','fontsize',16);
    xlabel('x [m]','fontsize',16);
    if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
        plot(omegaxis*sqrt(g0)/(2*pi),X(ix).*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'r','LineStyle',':')
    end
    axis([min(X) max(X) -4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi) 4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)  ]  )
    hold off;
    hc=colorbar;
    title(hc,'|B| [m]');
    ax1 = gca;
    ax1_pos = ax1.Position ;
    subplot(4,1,4);
    ax2 = gca;
    ax2_pos = ax2.Position;
    ax2_pos =  [ ax2_pos(1)  .5*ax2_pos(2)  .84*ax1_pos(3)  .7*ax2_pos(4)];
    set(ax2,'Position',ax2_pos);
    yline=H.*K;
    yline(end) = NaN;
    c = yline;
    patch(X,yline,c,'EdgeColor','interp','LineWidth',1.2,'MarkerFaceColor','flat');  
    ylabel('hk','fontsize',15);
    set(gca,'FontSize',15,'Ylim',[0.85*min(H.*K), 1.15*max(H.*K)],'ytick',[min(H.*K)  max(H.*K)*1.0000001],'XLim',[0. Lx],'xtick',[xvarstart,xvarstop]);
    ax2.XAxis.FontSize = 8;
    ylabel('hk ','fontsize',15);
    ytickformat('%.1f');
    xtickformat('%.1f');
    linkaxes([ax1, ax2], 'x');
    yyaxis(ax2, 'right');
    plot(X,-H,'--');
    y2fontsize=12;
    ylabel('h [m]','fontsize',y2fontsize);
    try
    ax2.YAxis(2).TickValues=sort([min(-H),max(-H)]);
    ax2.YAxis(2).FontSize = y2fontsize;
    ax2.YAxis(2).Exponent=-2;
    end
    ytickformat('%.1f');
    if Exportinit=='yes'
       savefig(strcat(img_folder,'\',flm,'_2d_fourier'))
	end
	%%
	f=figure('name','Fourier','position',[20 20 500 500]);
    hold on
    UdBth = -15;
    Usp = 20*log10(abs(uspectrum.')*sqrt(g0)*(1/(2*pi))/B0);
%     Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
    Usp = Usp-max(Usp,[],"all");
	Usp = (Usp>UdBth).*(Usp-UdBth)+UdBth;

	%     imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,abs(uspectrum')*sqrt(g0)*(1/(2*pi))/B0);
    imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,Usp);

	set(gca,'XDir','normal','FontSize',14);
%     ylim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);
    xlim([1,3.5]);
	ylim([X(1),X(end)])
    xlabel('f [Hz]','fontsize',16);
    ylabel('x [m]','fontsize',16);
    ytickformat('%.1f');
	colormap('jet')
	hold off
	colorbar()
	
	%%
% 	foudata=fftshift(ifft(u,[],1));
% 	L=length(foudata);
% 	ht=t(2)-t(1);% sampling frequency
% 	omegaxis = linspace(-1/ht/2,1/ht/2,L+1); omegaxis = omegaxis(1:end-1);
% 	new_omegaxis=omegaxis(L/2+1:end);
% 	raw_foudata=foudata(L/2+1:end,:)./max(foudata(L/2+1:end,:));
% 	Lx_raw=sqrt(length(u(:,1)).*2 ); %sqrt length timesample
% 	figure()
% 	pl_1=imagesc(new_omegaxis,X,abs(raw_foudata').*Lx_raw)
% 	ylim([X(1),X(end)])
%     xlim([1,3.5]);

	%%
       
    figure;
    meshc(t(tplot)/sqrt(g0),X,abs(u(tplot,:).')/B0);
    set(gca,'YDir','normal','FontSize',14);
    xlabel('T [s]','fontsize',16);
    ylabel('X [m]','fontsize',16);
    zlabel('|B|/B_0','fontsize',16);
    % shading interp;
    % colormap hot;

    figure('name','Steepness');
    meshc(t(tplot)/sqrt(g0),X,abs(u(tplot,:).').*(K.'*ones(size(t(tplot)))));
    set(gca,'YDir','normal','FontSize',14);
    xlabel('T [s]','fontsize',16);
    ylabel('X [m]','fontsize',16);
    zlabel('Steepness','fontsize',16);
    % shading interp;
    colormap hot;

    %%
    nu_tilde=Nu*Cg(1)./Cg;
    alpha_eta=-1.-Lambda.*((omega0)^2)./(nu_tilde.*B0^2);
    eta_MI=2/7.*(1.-alpha_eta);
    figure('name','alpha and eta');
    plot(X,alpha_eta,'k');
    ylabel('\alpha','fontsize',16);
    ax2 = gca;
    yyaxis right
    plot(X,eta_MI);
    ylabel('\eta MI','fontsize',16);

    %% tank plot
    f=figure('name','bathymetry','position',[20 20 600 200]);
    hold on
    ax=gca;
    plot(X,-H);
    plot(X,-1*ones(size(X)));    
    plot(X,-H(1)*ones(size(X)),':k');
    plot(X,0*ones(size(X)),'b');
    ang=atan((-H(length(H)-1)+H(1))/(xvarstop-xvarstart));
    angles=linspace(0,ang,25);
    amp=(xvarstop-xvarstart)*0.5;
    if max(X)>30
       plot([30 30],[0 -1],':k')
       if Exportinit=='yes'
           for j=1:length(pos_gau)
           plot([pos_gau(j) pos_gau(j)],[0 -min(H)],'-.k')
           end
        end
    end
    plot(amp*cos(angles)+xvarstart,amp*sin(angles)-H(1),'r' )
    hold off
    try
    if max(X)>30
       set(ax,'XLim',[0. Lx],'xtick',sort([0,xvarstart,xvarstop,30,Lx]) ) ;
    else
        set(ax,'XLim',[0. Lx],'xtick',sort([0,xvarstart,xvarstop,Lx]) ) ;
    end
    end
    ylabel('h [m]','fontsize',14);
    xlabel('x [m]','fontsize',14);
    angle=num2str(rad2deg(ang));
    legend({strcat('angle= ',angle) },'Location','southwest') ;
    try
    ax.YAxis.TickValues=sort([-1,min(-H),max(-H)]);
    end
    ax.YAxis.Exponent=-2;
    if Exportinit=='yes'
        savefig(strcat(img_folder,'\',flm,'_bathm'))
        filename_bathm=strcat(fullfolder,'\',flm,'_bathm.txt');
        M=[X' -H'];
        print_bathym_file2(filename_bathm,Input_function,sample_frequency,initial_depth,Factor,M)
    end
    
%%   
   
    if Exportinit=='yes'
    
  %%
  pos_error=zeros(length(pos_gau),1);
    figure('name','aprox signals');
    title('Simulated gauge signals','FontSize',15)
    plot_factor=20;
    hold on
    for j=1:length(pos_gau)
    pos_gau(j);
    [ d, ix ] = min( abs( X - pos_gau(j) ) );%find the critical value index
    if j==1
        ix1=ix;
    end
    X(ix);
    pos_error(j)=X(ix)-pos_gau(j);
    ix0 = find(tplot==1,1,'first');
    plot(t/sqrt(g0)-t(ix0)/sqrt(g0),abs(u(:,ix))*plot_factor+X(ix),'-b');    
    plot(t/sqrt(g0)-t(ix0)/sqrt(g0),...
        real(u(:,ix)'.*exp(1i*2*pi*Omega_dim.*(t/sqrt(g0)-t(ix0)/sqrt(g0)))).*plot_factor+X(ix),...
    '-r');   
    end  
    hold off 
    set(gca,'YMinorTick','on')
    xlim([0,30]);
    ylim([0,30]);
    grid minor
    ylabel('Distance to wave maker (m)','Interpreter','latex','fontsize',14);
    xlabel('${t-x/C_g}$ (s)','Interpreter','latex','fontsize',14);
    savefig(strcat(img_folder,'\',flm,'_sim_gauges'))
    %%
    [ d, ix ] = min( abs( t/sqrt(g0) - Out_time ) );
    fprintf( 'Usanity check: \n output time: %.2f is lower than half total time %.2f \n', Out_time, t(end)/sqrt(g0) );
    middle=round(length(t)*0.5);
    ix=round(abs(middle-ix)*0.5);
      j=3
    pos_gau(j);
    [ d, ix2 ] = min( abs( X - pos_gau(j) ) );%find the critical value index
    X(ix2);
    input_signal=Factor*real(u(middle-ix:middle+ix,ix2).*exp(1i*(Omega_dim*(2*pi)*t(middle-ix:middle+ix)/sqrt(g0)))');
    
    figure('name','Output sample');
    tp=t(middle-ix:middle+ix)/sqrt(g0);
    [A1,t_sample_try] = resample(input_signal,tp,100);
    t_sample=zeros(length(A1));
    t_sample=t_sample_try(1:length(A1));
    fprintf('sanity check: Wave maker sample frequency shoud be 0.01 \n sample frequency is:  %.3f \n ',t_sample(2)-t_sample(1))
%     A1=lowpass(A1,1.95);
    if A1(1)>0
        t0 = find(A1<0,1,'first');
        t1 = find(A1<0,1,'last');
    else
        t0 = find(A1>0,1,'first');
        t1 = find(A1>0,1,'last');
    end
    %borders at 0 (a tukey window might work also.)
    A2=[0 A1(t0:t1)' 0]';
    if t1==length(t_sample)
        t_sample(t1+1)=t_sample(t1);
    end
    t2= t_sample(t0-1:t1+1) ;
    M=[t2'-min(t2),A2];
%     csvwrite(filename_wm3,M) ;

    %%
  stokes_amp=0.5*esteep0^2/k0;
    pos_error=zeros(length(pos_gau),1);
    figure('name','aprox signals stks');
    title('Simulated gauge signals with stokes amplification','FontSize',15)
    plot_factor=20;  
    wmod=2*pi*Omega_dim;
    stk_sig_env=zeros(length(t),length(pos_gau));
    stk_sig=zeros(length(t),length(pos_gau));

    hold on
    for j=1:length(pos_gau)
    pos_gau(j);
    [ d, ix ] = min( abs( X - pos_gau(j) ) );%find the critical value index
    if j==1
        ix1=ix;
    end
    X(ix);
    pos_error(j)=X(ix)-pos_gau(j);
    ix0 = find(tplot==1,1,'first');
    u2=u(:,ix).*u(:,ix);
    stk_sig_env(:,j)=abs(u(:,ix)'.*exp(1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0)))+...
        0.5*k0*u2'.*exp(2*1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0))));
    stk_sig(:,j)=real(u(:,ix)'.*exp(1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0)))+...
        0.5*k0*u2'.*exp(2*1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0))));
      
    l1=plot(t/sqrt(g0)-t(ix0)/sqrt(g0),...
        stk_sig_env(:,j)*plot_factor+X(ix),...
        '--k');   %'color',[.7 .2 .0]
    set(l1,'LineWidth',.7)
    
    l2=plot(t/sqrt(g0)-t(ix0)/sqrt(g0),...
        stk_sig(:,j).*plot_factor+X(ix),...
    '-r');   
    set(l2,'LineWidth',.6)

    end  
    hold off 
    set(gca,'YMinorTick','on')
    xlim([0,30]);
    ylim([0,30]);
    grid minor
    ylabel('Distance to wave maker (m)','Interpreter','latex','fontsize',14);
    xlabel('${t-x/C_g}$ (s)','Interpreter','latex','fontsize',14);
    savefig(strcat(img_folder,'\',flm,'_sim_gauges_stokes'))
%     filename_bathm=strcat(fullfolder,'\',flm,'_bathm.txt');
%         M=[X' -H'];
%         print_bathym_file2(filename_bathm,Input_function,sample_frequency,initial_depth,Factor,M)

 %%
figure('name','Fourier signals')
 stokes_amp=0.5*esteep0^2/k0;
    pos_error=zeros(length(pos_gau),1);
    title('Simulated Fourier gauge signals \n with stokes amplification','FontSize',15)
    plot_factor=20;  
    wmod=2*pi*Omega_dim;
    hold on
    for j=1:length(pos_gau)
    pos_gau(j);
    [ d, ix ] = min( abs( X - pos_gau(j) ) );%find the critical value index
    if j==1
        ix1=ix;
    end
    X(ix);
    pos_error(j)=X(ix)-pos_gau(j);
    ix0 = find(tplot==1,1,'first');
    fa=1.127/((t(2)-t(1)));%why is it not right???????
    f = fa*(0:(L/2))/L;
%Define the frequency domain f and plot the single-sided amplitude spectrum P1. The amplitudes are not exactly at 0.7 and 1, as expected, because of the added noise.
% On average, longer signals produce better frequency approximations.
    P2 = abs(ifft(stk_sig_env(:,j))/L);
    P1 = P2(1:L/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
      
    P3= abs(ifft(stk_sig(:,j)) /L ) ;  
    P4 = P3(1:L/2+1);
    P4(2:end-1) = 2*P4(2:end-1);
    if j==1
      norm_four=max(P4);
    end
%     l1=plot(f*sqrt(g0),1.5*P1/max(P1)+X(ix),'--r');
    l2=plot(f,7*P4/norm_four+pos_gau(j),'-r');
    set(l2,'LineWidth',.8)

    end  

hold off
xlim([0,4])
% title('Evolution of the freq.','Interpreter','latex')
xlabel('f (Hz)','Interpreter','latex')    
    savefig(strcat(img_folder,'\',flm,'fourier_sims_stk'))


    %%
figure('name','Fourier signals')
 stokes_amp=0.5*esteep0^2/k0;
    pos_error=zeros(length(pos_gau),1);
    title('Simulated Fourier gauge signals','FontSize',15)
    plot_factor=20;  
    wmod=2*pi*Omega_dim;
    hold on
    for j=1:length(pos_gau)
    pos_gau(j);
    [ d, ix ] = min( abs( X - pos_gau(j) ) );%find the critical value index
    if j==1
        ix1=ix;
    end
    X(ix);
    pos_error(j)=X(ix)-pos_gau(j);
    ix0 = find(tplot==1,1,'first');
    fa=1.127/((t(2)-t(1)));%why is it not right???????
    f = fa*(0:(L/2))/L;
%Define the frequency domain f and plot the single-sided amplitude spectrum P1. The amplitudes are not exactly at 0.7 and 1, as expected, because of the added noise.
% On average, longer signals produce better frequency approximations.
    P3= abs(ifft( real(u(:,ix)'.*exp(1i*2*pi*Omega_dim.*(t/sqrt(g0)-t(ix0)/sqrt(g0))))) /L ) ;  
    P4 = P3(1:L/2+1);
    P4(2:end-1) = 2*P4(2:end-1);
    if j==1
      norm_four=max(P4);
    end
%     l1=plot(f*sqrt(g0),1.5*P1/max(P1)+X(ix),'--r');
    l2=plot(f,7*P4/norm_four+pos_gau(j),'-r');
    set(l2,'LineWidth',.8)

    end  

hold off
xlim([0,4])
% title('Evolution of the freq.','Interpreter','latex')
xlabel('f (Hz)','Interpreter','latex')    
    savefig(strcat(img_folder,'\',flm,'fourier_sims'))


    end
    
%%
    case 2

    %%        
    figure;
    Pos = [.14,.15,.79,.72];
    axes('Position',Pos);
    imagesc(t(abs(t)<=windowwidth),X,abs(u(tplot,:).')/B0);
    hold on;
    [~,nucross] = min(abs(H.*K-1.363));
    plot(t(tplot),xvarstart*ones(size(t(tplot))),'k:',t(tplot),xvarstop*ones(size(t(tplot))),'k:',t(tplot),X(nucross)*ones(size(t(tplot))),'m-.','Linewidth',2);
    set(gca,'YDir','normal','FontSize',14);
    xlabel('\tau [m^{1/2}]','fontsize',16);
    ylabel('\xi [m]','fontsize',16);
    %shading interp; colorbar;
    Pos = get(gca,'Position');
    Yl = get(gca,'ylim');
    % overlayed axis with kh changes
    axes('Position',Pos);
    plot(H.*K,X,'r--','linewidth',2);
    xlabel('kh','fontsize',16);
    set(gca,'XAxisLocation','top','GridAlpha',1,'YTickLabel','','Fontsize',14,'Ylim',Yl,'Color','none')
   
    f=figure('position',[20 20 500 500]);
    [ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
    pos1 = [0.05 0.3 0.9 0.65];
    subplot(4,1,[1 3]);
    % hold on 
    imagesc(X,t(tplot),abs(u(tplot,:)) );
     set(gca,'XDir','normal','FontSize',14,'xtick',[]);
     ylabel('\tau [m^{1/2}] ','fontsize',16);
    % plot(-X0.*ones(max(size(t(tplot)/sqrt(g0)))),t(tplot)/sqrt(g0),'--g')
    % plot(xvarstart.*ones(max(size(t(tplot)/sqrt(g0)))),t(tplot)/sqrt(g0),'color',[0.87,0.74,0.53],'LineStyle','--')
    % plot(xvarstop.*ones(max(size(t(tplot)/sqrt(g0)))),t(tplot)/sqrt(g0),'color',[0.87,0.74,0.53],'LineStyle','--')
    % if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
    %      plot(X(ix).*ones(max(size(t(tplot)/sqrt(g0)))),t(tplot)/sqrt(g0),'r','LineStyle',':')
    % end
    % hold off
    shading interp;
    hc=colorbar;
    title(hc,'|B|');
    ax1 = gca;
    ax1_pos = ax1.OuterPosition ;
    subplot(4,1,4);
    ax2 = gca;
    ax2_pos = ax2.OuterPosition;
    ax2_pos =  [ ax1_pos(1)  ax2_pos(2)  ax1_pos(3)  ax2_pos(4)];
    set(ax2,'OuterPosition',ax2_pos);
    yline=H.*K;
    yline(end) = NaN;
    c = yline;
    patch(X,yline,c,'EdgeColor','interp','LineWidth',1.2,'MarkerFaceColor','flat');
    % colorbar;
    xlabel('\xi [m]','fontsize',16);
    ylabel('hk','fontsize',16);
    set(gca,'XDir','normal','FontSize',14,'Ylim',[0.85*min(H.*K), 1.15*max(H.*K)],'ytick',[min(H.*K)  max(H.*K)*1.0000001],'XLim',[0. Lx]);
    ytickformat('%.1f');
          
    figure;
    meshc(t(tplot),X,abs(u(tplot,:).')/B0);
    set(gca,'YDir','normal','FontSize',14);
    xlabel('T [m^{1/2}]','fontsize',16);
    ylabel('X [m]','fontsize',16);
    zlabel('|B|/B_0','fontsize',16);
    % shading interp;
    % colormap hot;

    figure;
    meshc(t(tplot),X,abs(u(tplot,:).').*(K.'*ones(size(t(tplot)))));
    set(gca,'YDir','normal','FontSize',14);
    xlabel('T [m^{1/2}]','fontsize',16);
    ylabel('X [m]','fontsize',16);
    zlabel('Steepness','fontsize',16);
    % shading interp;
    colormap hot;

    % spectrum map
    figure;
    UdBth = -60;
    Usp = 20*log10(abs(uspectrum.')/B0);
    Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
    imagesc(omegaxis,X,Usp);
    hold on;
    plot(omegaxis,xvarstart*ones(size(omegaxis)),'k:',omegaxis,xvarstop*ones(size(omegaxis)),'k:','Linewidth',2);
    set(gca,'YDir','normal','FontSize',14);
    xlim([-4*omega0,4*omega0]);
    xlabel('\omega [m^{-1/2}]','fontsize',16);
    ylabel('X [m]','fontsize',16);
    % shading interp;
    % colormap hot;
    colorbar;
    Pos = get(gca,'Position');
    Yl = get(gca,'ylim');
    axes('Position',Pos);
    plot(H.*K,X,'r--','linewidth',2);
    xlabel('hk','fontsize',16);
    set(gca,'XAxisLocation','top','GridAlpha',1,'YTickLabel','','Fontsize',14,'Ylim',Yl,'Color','none')

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% benchmark plots


%%
f=figure('name','Fourier','position',[20 20 500 500]);
    hold on
    UdBth = -15;
    Usp = 10*log10(abs(uspectrum.').*sqrt(length(u(:,1)).*2)*sqrt(g0)*(1/(2*pi)));
% 	    Usp = 10*log10(abs(uspectrum.')./max(uspectrum,[],"all"));

% 	    Usp = 20*log10(abs(uspectrum.').*sqrt(length(u(:,1)).*2)*sqrt(g0)*(1/(2*pi))./B0);
%     Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
    Usp = Usp-max(Usp,[],"all");
	Usp = (Usp>UdBth).*(Usp-UdBth)+UdBth;

	%     imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,abs(uspectrum')*sqrt(g0)*(1/(2*pi))/B0);
    imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,Usp);

	set(gca,'XDir','normal','FontSize',14);
%     ylim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);
    xlim([1,3.5]);
	ylim([X(1),X(end)])
    xlabel('f [Hz]','fontsize',16);
    ylabel('x [m]','fontsize',16);
    ytickformat('%.1f');
	colorMap = jet(256);
	colormap(colorMap);   % Apply the colormap
	hold off
	colorbar()
	
	
		%%
	L0=2*pi./k0;
    f=figure('position',[20 20 500 500]);
    s=pcolor(t(tplot)./sqrt(g0),fliplr(X),flipud(abs(u(tplot,:)')));
	set(s, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
% 	set(s,'ytick',[0 20 ] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})
% 	set(gca,'ytick',[0 20 40 60],'yticklabel',{0 20 40 60},'xtick',[-16 -8 0 8 16],'xticklabel',{-16 -8 0 8 16})
hold on
plot([t(1)./sqrt(g0) t(end)./sqrt(g0)],[4 4],'--g')
hold off
     xlabel('t[s] ','fontsize',16); 
	 ylabel('x[m]  ','fontsize',16);
	 xlim([-10,10])
	 ylim([0,10])
	colorMap = jet(256);
	colormap(colorMap);   % Apply the colormap
	cb=colorbar();
	ylabel(cb, '|U|')
% 	 	caxis([0 8])

 	caxis([0 8*10^(-3)])
	
	%%
	L0=2*pi./k0;
    f=figure('position',[20 20 500 500]);
    s=pcolor(t(tplot)./T0,fliplr(X./L0),flipud(abs(u(tplot,:)'))./B0);
	set(s, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
% 	set(s,'ytick',[0 20 ] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})
	set(gca,'ytick',[0 20 40 60],'yticklabel',{0 20 40 60},'xtick',[-16 -8 0 8 16],'xticklabel',{-16 -8 0 8 16})

     xlabel('\tau/T_0  ','fontsize',16); 
	 ylabel('x/L_{0}  ','fontsize',16);
	 xlim([-16,16])
	 ylim([0,60])
	colorMap = jet;
	colormap(colorMap);   % Apply the colormap
	cb=colorbar();
	ylabel(cb, '|U|/a_0')
	caxis([0 2])
	
	
	
%%

function print_input_file2(filename,Input_function,input_freq,initial_depth,wm_factor,M)
fileID = fopen(filename,'w');

format shortg;
datetime = clock;
fprintf(fileID,'#EDL\tDate\t%4d.%2d.%2d\tLocal Time\t%2d:%2d:%.3f\tDN_001\n',datetime);
fprintf(fileID,'Rate(Hz)\tinput_function\tinitial_depth_in_m\twm_factor\n');

fprintf(fileID,'%.1f\t%',input_freq);
fprintf(fileID,Input_function);
fprintf(fileID,'\t%.2f\t%',initial_depth);
fprintf(fileID,'%.2f\n%',wm_factor);

fprintf(fileID,'Time\tInput_values\n');
fprintf(fileID,'s\tm\n');

% #EDL	Date	17.10.2018	Local Time	13:46:24	DN_001	
% Rate (Hz)	paddles			Main array	
fprintf(fileID,'%5d\t%5d\n',M');
fclose(fileID);
end

function print_bathym_file2(filename,Input_function,input_freq,initial_depth,wm_factor,M)
fileID = fopen(filename,'w');

format shortg;
datetime = clock;
fprintf(fileID,'#EDL\tDate\t%4d.%2d.%2d\tLocal Time\t%2d:%2d:%.3f\tDN_001\n',datetime);
fprintf(fileID,'Rate(Hz)\tinput_function\tinitial_depth_in_m\twm_factor\n');

fprintf(fileID,'%.1f\t%',input_freq);
fprintf(fileID,Input_function);
fprintf(fileID,'\t%.2f\t%',initial_depth);
fprintf(fileID,'%.2f\n%',wm_factor);

fprintf(fileID,'X\tH\n');
fprintf(fileID,'m\tm\n');	
fprintf(fileID,'%5d\t%5d\n',M');
fclose(fileID);
end

function print_pos_file(filename,Input_function,input_freq,initial_depth,wm_factor,M)
fileID = fopen(filename,'w');

format shortg;
datetime = clock;
fprintf(fileID,'#EDL\tDate\t%4d.%2d.%2d\tLocal Time\t%2d:%2d:%.3f\tpos\n',datetime);
fprintf(fileID,'Rate(Hz)\tinput_function\tinitial_depth_in_m\twm_factor\n');

fprintf(fileID,'%.1f\t%',input_freq);
fprintf(fileID,Input_function);
fprintf(fileID,'\t%.2f\t%',initial_depth);
fprintf(fileID,'%.2f\n%',wm_factor);

fprintf(fileID,'gauges_position\tdepth\n');
fprintf(fileID,'m\tm\n');	
fprintf(fileID,'%.5f\t\n',M);
fclose(fileID);
end

function [flm fullfolder] =save_filename(Omega_dim,B0,esteepness,T0_dim)
    currentFolder = pwd;
    folder = strcat(currentFolder,'\data\');
    format shortg;
    datetime = clock;
    date= sprintf('%4d_%.2i_%.2i-%.2i_%.2i_%.1f',datetime);
    flm=sprintf('_W_%.4f_bo_%.4f_es_%.3f_T0_%.4f',Omega_dim,B0,esteepness,T0_dim);
    flm=strcat(date,flm);
    flm = strrep(flm, '.', 'p');
    fullfolder = strcat(folder,flm);
end
function print_params_file(filename,Input_function,input_freq, save_params,save_params_names);%TO BE USED TO SAVE PARAMETERS TO FILE. 

fileID = fopen(filename,'w');

format shortg;
datetime = clock;
fprintf(fileID,'#EDL\tDate\t%4d.%2d.%2d\tLocal Time\t%2d:%2d:%.3f\tparams\n',datetime);
fprintf(fileID,'Rate(Hz)\tinput_function\tinput_frequency\n');
fprintf(fileID,'%.1f\t%\n',input_freq);
fprintf(fileID,'\n')

for j=1:length(save_params_names)
    fprintf(fileID,'%s\t',string(save_params_names(j)));
end
fprintf(fileID,'\n')
for j=1:length(save_params)
    fprintf(fileID,'%s\t',string(save_params(j)));
end
fprintf(fileID,'\n')

       
end



